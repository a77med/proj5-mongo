"""
Replacement for RUSA ACP brevet time calculator
(see https://rusa.org/octime_acp.html)

"""

import flask
import os
from flask import Flask, redirect, url_for, request, render_template
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config
import pymongo
from pymongo import MongoClient
import logging

###
# Globals
###
app = flask.Flask(__name__)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY

#Creating a client object
client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)

#Connecting to the db
db = client.timesdb

# Empty DB
db.timesdb.delete_many({})

###
# Pages
###

@app.route("/display", methods=["POST"])
def display():
    #Find items in timesdb
    item_find = db.timesdb.find()
    #fill the list with items of timesdb
    items = [item for item in item_find]
    # if the list is empty, dont do anything
    # This is better than redirecting
    # since the user will not have to go back or do anything.
    if items == []:
        return ("", 204)
    # Else, show times in a new html times.html
    else:
        return render_template("times.html", items=items)


@app.route("/submit", methods=['POST'])
def submit():
    # Empty DB
    db.timesdb.delete_many({})
    
    # Get Data list
    open_data = request.form.getlist("open")
    close_data = request.form.getlist("close")
    km_data = request.form.getlist("km")
    miles_data = request.form.getlist("miles")
    
    # Get fixed data
    begin_date = request.form.get('begin_date', type=str)
    begin_time = request.form.get('begin_time', type=str)

    # Initialize empty lists for list based data
    open_list = []
    close_list = []
    km_list = []
    miles_list = []
    checknum_list = []

    # Append open times data in its list
    for element in open_data:
        if str(element) != "":
            open_list.append(str(element))

    # Append close times data in its list
    for	element in close_data:
        if str(element) != "":
            close_list.append(str(element))
            
    # Append km data in its list
    for	element in km_data:
        if str(element) != "":
            km_list.append(str(element))

    # Append miles data in its list
    for	element in miles_data:
        if str(element)	!= "":
            miles_list.append(str(element))
            

    # Append all the lists and elements in a
    # key:values format.
    for i in range(len(km_list)):
        checknum_list.append(int(i))
        lists = {
            "open_time" : open_list[i],
            "close_time" : close_list[i],
            "km" : km_list[i],
            "miles" : miles_list[i],
            "begin_date" : begin_date,
            "begin_time" : begin_time,
            "checknum" : checknum_list[i]
        }
        # Insert the info, one by one
        # based on rows
        db.timesdb.insert_one(lists)

    # nothing to return, just saving
    return ("", 204)
    #return redirect(url_for("submit"))
    ###########################################

        
@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404


###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    km = request.args.get('km', 999, type=float)
    # Added the begin_date, begin_time, brevet_dist_km to be passed
    begin_date = request.args.get('begin_date', 999,type=str)
    begin_time = request.args.get('begin_time', 999, type=str)
    brevet_dist_km = request.args.get('brevet_dist_km', 999, type=int)
    
    
    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))
 
    #this allows the time to be connected and split by a T for format
    time_connect = "{}T{}".format(begin_date, begin_time)
    time = arrow.get(time_connect)
    # Timezone must be noted
    time = time.replace(tzinfo = 'US/Pacific')
    # Making sure it is isoformat
    time = time.isoformat() 
    open_time = acp_times.open_time(km, brevet_dist_km, time)
    close_time = acp_times.close_time(km, brevet_dist_km, time)

    #open_time = acp_times.open_time(km, 200, arrow.now().isoformat())
    #close_time = acp_times.close_time(km, 200, arrow.now().isoformat())
    
    result = {"open": open_time, "close": close_time}
    return flask.jsonify(result=result)


    


#############

app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    print("Opening for global access on port {}".format(CONFIG.PORT))
    app.run(port=CONFIG.PORT, host="0.0.0.0")
    app.debug = True
