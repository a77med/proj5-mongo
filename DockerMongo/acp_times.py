"""OA
Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders
"""
import arrow

#  Note for CIS 322 Fall 2016:
#  You MUST provide the following two functions
#  with these signatures, so that I can write
#  automated tests for grading.  You must keep
#  these signatures even if you don't use all the
#  same arguments.  Arguments are explained in the
#  javadoc comments.
#


def open_time(control_dist_km, brevet_dist_km, brevet_start_time):
    if (brevet_dist_km < control_dist_km):
        limit_km = brevet_dist_km*1.2
        if (limit_km) >= (control_dist_km):
            if brevet_dist_km == 200:
                #Get the date and time designated by the user                                                                                                             
                arw = arrow.get(brevet_start_time)
                max_speed = 34
            
                # Float so we can calculate the minutes                                                                                                                   
                distance_speed = float(200/max_speed)
                # Gets rid of the floating point                                                                                                                         
                hours = int(distance_speed)
                # Subtract the hours (whole numbers) and multiply by 60 to get minute represenataion                                                                      
                minutes = round(float(distance_speed - hours)*60)
                
                # Shift original time this much                                                                                                                          
                arw = arw.shift(hours=hours, minutes=minutes)
                
                # return the updated time                                                                                                                                
                return arw.isoformat()

            elif brevet_dist_km == 300:
                #Get the date and time designated by the user                                                                                            
                arw = arrow.get(brevet_start_time)
                max_speed = 32
                remainder = control_dist_km-200
		
                # Float so we can calculate the minutes                                                                                                                   
                distance_speed = float(200/34)+float(100/max_speed)
                # Gets rid of the floating point                                                                                                                          
                hours = int(distance_speed)
                # Subtract the hours (whole numbers) and multiply by 60 to get minute represenataion                                                                      
                minutes = round(float(distance_speed - hours)*60)

                # Shift original time this much                                                                                                                           
                arw = arw.shift(hours=hours, minutes=minutes)
                # return the updated time                                                                                                                                 
                return arw.isoformat()

            
            elif brevet_dist_km == 400:
                #Get the date and time designated by the user                                                                                                             
                arw = arrow.get(brevet_start_time)
                max_speed = 32
                remainder = control_dist_km-200
                
                # Float so we can calculate the minutes                                                                                                                  
                distance_speed = float(200/34)+float(200/max_speed)
                # Gets rid of the floating point                                                                                                                         
                hours = int(distance_speed)
                # Subtract the hours (whole numbers) and multiply by 60 to get minute represenataion                                                                   
                minutes = round(float(distance_speed - hours)*60)

                # Shift original time this much                                                                                                                        
                arw = arw.shift(hours=hours, minutes=minutes)
                # return the updated time                                                                                                                               
                return arw.isoformat()
            
            elif brevet_dist_km == 600:
                #Get the date and time designated by the user                                                                                                             
                arw = arrow.get(brevet_start_time)
                max_speed = 30
                remainder = control_dist_km - 400
                
                # Float so we can calculate the minutes                                                                                                               
                distance_speed =float(200/34)+float(200/32)+float(200/max_speed)
                # Gets rid of the floating point                                                                                                                          
                hours = int(distance_speed)
                # Subtract the hours (whole numbers) and multiply by 60 to get minute represenataion                                                                     
                minutes = round(float(distance_speed - hours)*60)
        
                # Shift original time this much                                                                                                                          
                arw = arw.shift(hours=hours, minutes=minutes)
                # return the updated time
                return arw.isoformat()

            elif brevet_dist_km == 1000:
                #Get the date and time designated by the user                                                                                                             
                arw = arrow.get(brevet_start_time)
                max_speed = 28
                remainder = control_dist_km - 600
                
                # Float so we can calculate the minutes                                                                                                               
                distance_speed = float(200/34)+float(200/32)+float(200/30)+float(400/max_speed)
                # Gets rid of the floating point                                                                                                                      
                hours = int(distance_speed)
                # Subtract the hours (whole numbers) and multiply by 60 to get minute represenataion                                                             
                minutes = round(float(distance_speed - hours)*60)
                
                # Shift original time this much                                                                                                                        
                arw = arw.shift(hours=hours, minutes=minutes)
                # return the updated time                                                                                                                              
                return arw.isoformat()
            
            
            else:
                # not supposed to happen
                return arrow.get(brevet_start_time).isoformat()
        else:
            # not supposed to happen
            return arrow.get(brevet_start_time).isoformat()

    elif control_dist_km == 0:
        arw = arrow.get(brevet_start_time)
        return arw.isoformat()
        
    elif (control_dist_km > 0 and control_dist_km <= 200):
        if (control_dist_km > 0) and (control_dist_km <= 60):
            #Get the date and time designated by the user                                                                                                                 
            arw = arrow.get(brevet_start_time)
            max_speed = 34
            
            # Float so we can calculate the minutes                                                                                                                       
            distance_speed = float(control_dist_km/max_speed)
            # Gets rid of the floating point                                                                                                                              
            hours = int(distance_speed)
            # Subtract the hours (whole numbers) and multiply by 60 to get minute represenataion                                                                          
            minutes = round(float(distance_speed - hours)*60)
            # Shift original time this much
            
            arw = arw.shift(hours=hours, minutes=minutes)
            # return the updated time                                                                                                                                     
            return arw.isoformat()
        
        else:
            #Get the date and time designated by the user
            arw = arrow.get(brevet_start_time)
            max_speed = 34
            
            # Float so we can calculate the minutes
            distance_speed = float(control_dist_km/max_speed)
            # Gets rid of the floating point
            hours = int(distance_speed)
            # Subtract the hours (whole numbers) and multiply by 60 to get minute represenataion
            minutes = round(float(distance_speed - hours)*60)
            
            # Shift original time this much
            arw = arw.shift(hours=hours, minutes=minutes)
            # return the updated time
            return arw.isoformat()
        
        
    elif (control_dist_km > 200 and control_dist_km <= 400):
        #Get the date and time designated by the user                                                                                                                     
        arw = arrow.get(brevet_start_time)
        max_speed = 32
        remainder = control_dist_km-200
        
        # Float so we can calculate the minutes                                                                                                                       
        distance_speed = float(200/34)+float(remainder/max_speed)
        # Gets rid of the floating point                                                                                                                             
        hours = int(distance_speed)
        # Subtract the hours (whole numbers) and multiply by 60 to get minute represenataion                                                                         
        minutes = round(float(distance_speed - hours)*60)
        
        # Shift original time this much                                                                                                                                   
        arw = arw.shift(hours=hours, minutes=minutes)
        # return the updated time                                                                                                                                         
        return arw.isoformat()
    
    elif (control_dist_km > 400 and control_dist_km <= 600):
        #Get the date and time designated by the user                                                                                                                     
        arw = arrow.get(brevet_start_time)
        max_speed = 30
        remainder = control_dist_km - 400
        
        # Float so we can calculate the minutes                                                                                                                      
        distance_speed =float(200/34)+float(200/32)+float(remainder/max_speed)
        # Gets rid of the floating point                                                                                                                                  
        hours = int(distance_speed)
        # Subtract the hours (whole numbers) and multiply by 60 to get minute represenataion                                                                              
        minutes = round(float(distance_speed - hours)*60)
        
        # Shift original time this much                                                                                                                                   
        arw = arw.shift(hours=hours, minutes=minutes)
        # return the updated time                                                                                                                                         
        return arw.isoformat()
    
    
    elif (control_dist_km > 600 and control_dist_km <= 1000):
        #Get the date and time designated by the user                                                                                                                     
        arw = arrow.get(brevet_start_time)
        max_speed = 28
        remainder = control_dist_km - 600
        
        # Float so we can calculate the minutes                                                                                                                           
        distance_speed = float(200/34)+float(200/32)+float(200/30)+float(remainder/max_speed)
        # Gets rid of the floating point                                                                                                                                  
        hours = int(distance_speed)
        # Subtract the hours (whole numbers) and multiply by 60 to get minute represenataion                                                                              
        minutes = round(float(distance_speed - hours)*60)
        
        # Shift original time this much                                                                                                                                   
        arw = arw.shift(hours=hours, minutes=minutes)
        
        # return the updated time                                                                                                                                         
        return arw.isoformat()
    
    
    else:
        return arrow.now().isoformat()
    
    
    
    
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
       brevet_dist_km: number, the nominal distance of the brevet
           in kilometers, which must be one of 200, 300, 400, 600,
           or 1000 (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control open time.
       This will be in the same time zone as the brevet start time.
    """
    return arrow.now().isoformat()


def close_time(control_dist_km, brevet_dist_km, brevet_start_time):
    if (brevet_dist_km <= control_dist_km):
        limit_km = brevet_dist_km*1.2
        if (limit_km) >= (control_dist_km):
            if brevet_dist_km == 200:
                #Get the date and time designated by the user                                                                                                             
                arw = arrow.get(brevet_start_time)
                max_speed = 15

                # Float so we can calculate the minutes                                                                                                                   
                #distance_speed = float(200/max_speed)
                # Gets rid of the floating point                                                                                                                          
                #hours = int(distance_speed)
                # Subtract the hours (whole numbers) and multiply by 60 to get minute represenataion                                                                      
                #minutes = round(float(distance_speed - hours)*60)

                # Shift original time this much                                                                                                                           
                arw = arw.shift(hours=+13, minutes=+30)
                # return the updated time                                                                                                                                 
                return arw.isoformat()
            
            elif brevet_dist_km == 300:
                #Get the date and time designated by the user                                                                                                             
                arw = arrow.get(brevet_start_time)
                min_speed = 15
                remainder = control_dist_km-200

                # Float so we can calculate the minutes                                                                                                                   
                #distance_speed = float(300/min_speed)
                # Gets rid of the floating point                                                                                                                          
                #hours = int(distance_speed)
                # Subtract the hours (whole numbers) and multiply by 60 to get minute represenataion                                                                      
                #minutes = round(float(distance_speed - hours)*60)

                # Shift original time this much                                                                                                                           
                arw = arw.shift(hours=20)
                # return the updated time                                                                                                                                 
                return arw.isoformat()
            
            elif brevet_dist_km == 400:
                #Get the date and time designated by the user                                                                                                             
                arw = arrow.get(brevet_start_time)
                min_speed = 15
                remainder = control_dist_km-200

                # Float so we can calculate the minutes                                                                                                               
                #distance_speed = float(400/min_speed)
                # Gets rid of the floating point                                                                                                                      
                #hours = int(distance_speed)
                # Subtract the hours (whole numbers) and multiply by 60 to get minute represenataion                                                                    
                #minutes = round(float(distance_speed - hours)*60)

                # Shift original time this much                                                                                                                           
                arw = arw.shift(hours=27)
                # return the updated time                                                                                                                            
                return arw.isoformat()

            elif brevet_dist_km == 600:
         	#Get the date and time designated by the user                                                                                                             
                arw = arrow.get(brevet_start_time)
                min_speed = 15
                remainder = control_dist_km-200

		# Float so we can calculate the minutes                                                                                                             
                #distance_speed = float(600/min_speed)
		# Gets rid of the floating point                                                                                                                       
                #hours = int(distance_speed)
		# Subtract the hours (whole numbers) and multiply by 60 to get minute represenataion                                                                
                #minutes = round(float(distance_speed - hours)*60)
                
                # Shift original time this much                                                                                                                           
                arw = arw.shift(hours=40)
                # return the updated time                                                                                                                            
                return arw.isoformat()
            
            elif brevet_dist_km == 1000:
                #Get the date and time designated by the user                                                                                             
                arw = arrow.get(brevet_start_time)
                min_speed = 15
                remainder = control_dist_km-200
                
                # Float so we can calculate the minutes                                                                                                                   
                #distance_speed = float(600/15)+float(400/11.428)
                # Gets rid of the floating point                                                                                                                          
                #hours = int(distance_speed)
                # Subtract the hours (whole numbers) and multiply by 60 to get minute represenataion                                                                      
                #minutes = round(float(distance_speed - hours)*60)
                
                # Shift original time this much                                                                                                                           
                arw = arw.shift(hours=75)
                # return the updated time                                                                                                                                 
                return arw.isoformat()
            else:
                # not supposed to happen                                                                                                                                  
                return arrow.get(brevet_start_time).isoformat()
        else:
            # not supposed to happen                                                                                                                        
            return arrow.get(brevet_start_time).isoformat()
            
    elif control_dist_km == 0:
        #Get the date and time designated by the user                                                                                                                 
        arw = arrow.get(brevet_start_time)
        
        #Shift original time this much                                                                                                                                 
        arw = arw.shift(hours=1)
        
        #return the updated time                                                                                                                                       
        return arw.isoformat()
    
    elif (control_dist_km > 0 and control_dist_km <= 200):
        if (control_dist_km > 0) and (control_dist_km <= 60):
	    #Get the date and time designated by the user                                                                                                                 
            arw = arrow.get(brevet_start_time)
            min_speed = 20
            
            # Float so we can calculate the minutes                                                                                                                       
            distance_speed = float(control_dist_km/min_speed)
            # Gets rid of the floating point                                                                                                                              
            hours = int(distance_speed)
            # Subtract the hours (whole numbers) and multiply by 60 to get minute represenataion                                                                          
            minutes = round(float(distance_speed - hours)*60)
            # Shift original time this much                                                                                                                               
            arw = arw.shift(hours=hours+1, minutes=minutes)
            # return the updated time                                                                                                                                     
            return arw.isoformat()
        
        else:
            #Get the date and time designated by the user                                                                                                                 
            arw = arrow.get(brevet_start_time)
            min_speed = 15
            
            # Float so we can calculate the minutes                                                                                                                      
            distance_speed = float(control_dist_km/min_speed)
            # Gets rid of the floating point                                                                                                                             
            hours = int(distance_speed)
            # Subtract the hours (whole numbers) and multiply by 60 to get minute represenataion                                                                          
            minutes = round(float(distance_speed - hours)*60)
            
            #Shift original time this much                                                                                                                               
            arw = arw.shift(hours=hours, minutes=minutes)
            #return the updated time                                                                                                                                     
            return arw.isoformat()
        
        
        
    elif (control_dist_km > 200 and control_dist_km <= 400):
        #Get the date and time designated by the user                                                                                                                     
        arw = arrow.get(brevet_start_time)
        min_speed = 15
        
        # Float so we can calculate the minutes                                                                                                                           
        distance_speed = float(control_dist_km/min_speed)
        # Gets rid of the floating point                                                                                                                                  
        hours = int(distance_speed)
        # Subtract the hours (whole numbers) and multiply by 60 to get minute represenataion                                                                              
        minutes = round(float(distance_speed - hours)*60)
        
        # Shift original time this much                                                                                                                                   
        arw = arw.shift(hours=hours, minutes=minutes)
        # return the updated time                                                                                                                                         
        return arw.isoformat()
    
    elif (control_dist_km > 400 and control_dist_km <= 600):
        
        #Get the date and time designated by the user                                                                                                                     
        arw = arrow.get(brevet_start_time)
        min_speed = 15
        
        # Float so we can calculate the minutes                                                                                                                           
        distance_speed = float(control_dist_km/min_speed)
        # Gets rid of the floating point                                                                                                                                  
        hours = int(distance_speed)
        # Subtract the hours (whole numbers) and multiply by 60 to get minute represenataion                                                                              
        minutes = round(float(distance_speed - hours)*60)
        
        # Shift original time this much                                                                                                                                   
        arw = arw.shift(hours=hours, minutes=minutes)
        # return the updated time                                                                                                                                         
        return arw.isoformat()
    
    
    elif (control_dist_km > 600 and control_dist_km <= 1000):
        
        #Get the date and time designated by the user                                                                                                                     
        arw = arrow.get(brevet_start_time)
        min_speed = 11.428
        remainder = control_dist_km - 600
        
        # Float so we can calculate the minutes                                                                                                                           
        distance_speed = float(600/15)+float(remainder/min_speed)
        # Gets rid of the floating point                                                                                                                                  
        hours = int(distance_speed)
        # Subtract the hours (whole numbers) and multiply by 60 to get minute represenataion                                                                              
        minutes = round(float(distance_speed - hours)*60)
        
        # Shift original time this much                                                                                                                                   
        arw = arw.shift(hours=hours, minutes=minutes)
        # return the updated time                                                                                                                                         
        return arw.isoformat()
    
    
    else:
        return arrow.get(brevet_start_time).isoformat()
    
    
    """
    Args:
    control_dist_km:  number, the control distance in kilometers
    brevet_dist_km: number, the nominal distance of the brevet
    in kilometers, which must be one of 200, 300, 400, 600, or 1000
    (the only official ACP brevet distances)
    brevet_start_time:  An ISO 8601 format date-time string indicating
    the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control close time.
       This will be in the same time zone as the brevet start time.
    """
    #return arrow.now().isoformat()

