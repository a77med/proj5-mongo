# Project 5: Brevet time calculator with Ajax and MongoDB

Simple list of controle times from project 4 stored in MongoDB database.

As of now. the Function of this program is running corretly.

##Submit button:

Returns no content, since it is just writing into our MongoDB.
Therefore, I used the return ("",204) which returns code :No Content.

In any case, however, no new window will pop up, since it is not necessary.
The user can hit submit any number of times with no error.

##Display button:

The display error handles things differently.

If there is content that is added, then it will redirect to times.html
which will print out the table. Prints are almost symmetrical to the input
table, with the correct values.

If there is not content, however, the button will not anything, and
it will still allow the user to input data and submit again.
-----------
Error handling as well as data transfer seems to be intact and functioning after
continuous testing.